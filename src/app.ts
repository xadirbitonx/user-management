import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import {createIndexRoute} from "./routes/index";
import {createUsersRoute} from "./routes/users";
import {createGroupsRoute} from "./routes/groups";
import {UsersModule} from "./modules/users/users-module";
import {GroupsModule} from "./modules/groups/groups-module";

function getApp() {
    const app = express();

    const corsOptions = {
        origin: 'http://localhost:4200',
        optionsSuccessStatus: 200
    };
    app.use(cors(corsOptions));
    app.use(express.json());
    app.use(express.urlencoded({extended: false}));

    // Routes
    app.use('/', createIndexRoute());
    app.use('/users', createUsersRoute());
    app.use('/groups', createGroupsRoute());

    return app;
}

async function initServer(): Promise<number> {
    // connect to db
    await mongoose.connect('mongodb://localhost/user-management');
    console.log("connected to db");

    // init modules
    UsersModule.init();
    GroupsModule.init();

    return 5000;
}

async function startApp(): Promise<void> {
    const port = await initServer();
    const app = getApp();

    app.listen(port, () => {
        console.log("listening on port " + port);
    });
}

startApp().catch(err => {
    console.log("failed starting app");
    process.exit(1);
});
