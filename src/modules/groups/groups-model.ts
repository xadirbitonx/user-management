import * as mongoose from "mongoose";
import {Model, Schema} from "mongoose";

export class GroupsModel {
    private static model: Model<IGroup>;

    public constructor() {
        GroupsModel.model = mongoose.model<IGroup>('groups', groupSchema);
    }

    public async changeGroupStatus(groupId: string, status: GroupStatus): Promise<void> {
        await GroupsModel.model.findOneAndUpdate({groupId}, {status: status}).exec();
    }

    public async addGroup(groupName: string): Promise<void> {
        await GroupsModel.model.create({name: groupName});
    }
}

export enum GroupStatus {
    Empty = 0,
    NotEmpty = 1
}
export interface IGroup {
    name: string,
    status: GroupStatus
}

const groupSchema = new Schema<IGroup>({
    name: String,
    status: {type: Number, default: GroupStatus.Empty}
});
