import {GroupsModel, GroupStatus, IGroup} from "./groups-model";
import {UsersModule} from "../users/users-module";

export class GroupsModule {
    private static groupsModel: GroupsModel;

    public static init() {
        GroupsModule.groupsModel = new GroupsModel();
    }

    public static async changeGroupStatus(groupId: string, status: GroupStatus): Promise<void> {
        await GroupsModule.groupsModel.changeGroupStatus(groupId, status);
    }

    public static async isGroupEmpty(groupId: string): Promise<boolean> {
        const usersByGroup = await UsersModule.getUsersByGroup(groupId);
        return usersByGroup.length == 0;
    }

    public static async addGroup(groupName: string): Promise<void> {
        await GroupsModule.groupsModel.addGroup(groupName);
    }
}