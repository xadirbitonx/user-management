import * as mongoose from "mongoose";
import {Model, Schema} from "mongoose";

export class UsersModel {
    private static model: Model<IUser>;

    public constructor() {
        UsersModel.model = mongoose.model<IUser>('users', userSchema);
    }

    public async getUsers(limit: number, offset: number): Promise<IUser[]> {
        return await UsersModel.model.find({}).limit(limit).skip(offset).exec();
    }

    public async getUsersByName(name: string): Promise<IUser[]> {
        return await UsersModel.model.find({name: name}).exec();
    }

    public async getUsersByEmail(email: string): Promise<IUser[]> {
        return await UsersModel.model.find({email: email}).exec();
    }

    public async getUsersByGroup(groupId: string): Promise<IUser[]> {
        return await UsersModel.model.find({groupId: groupId}).exec();
    }

    public async removeUserFromGroup(userId: string): Promise<IUser> {
        const user = await UsersModel.model.findOneAndUpdate({userId}, {groupId: ""}, {returnOriginal: true}).exec();
        if (!user) {
            throw new Error("no user with that id");
        }
        return user;
    }

    public async addUser(user: IUser): Promise<IUser> {
        return await UsersModel.model.create(user);
    }
}

export interface IUser {
    groupId: string,
    name: string,
    email: string
}

const userSchema = new Schema<IUser>({
    groupId: String,
    name: String,
    email: String
});
