import {UsersModel, IUser} from "./users-model";
import {GroupsModule} from "../groups/groups-module";
import {GroupStatus} from "../groups/groups-model";

export class UsersModule {
    private static usersModel: UsersModel;

    public static init() {
        UsersModule.usersModel = new UsersModel();
    }

    public static async getUsers(limit: number, offset: number): Promise<IUser[]> {
        return await UsersModule.usersModel.getUsers(limit, offset);
    }

    public static async getUsersByName(name: string): Promise<IUser[]> {
        return await UsersModule.usersModel.getUsersByName(name);
    }

    public static async getUsersByEmail(email: string): Promise<IUser[]> {
        return await UsersModule.usersModel.getUsersByEmail(email);
    }

    public static async getUsersByGroup(groupId: string): Promise<IUser[]> {
        return await UsersModule.usersModel.getUsersByGroup(groupId);
    }

    public static async removeUserFromGroup(userId: string): Promise<void> {
        const user = await UsersModule.usersModel.removeUserFromGroup(userId);
        const isGroupEmpty = await GroupsModule.isGroupEmpty(user.groupId);

        if (isGroupEmpty) {
            await GroupsModule.changeGroupStatus(user.groupId, GroupStatus.Empty);
        }
    }

    public static async addUser(user: IUser): Promise<void> {
        const newUser = await UsersModule.usersModel.addUser(user);
        await GroupsModule.changeGroupStatus(newUser.groupId, GroupStatus.NotEmpty);
    }
}