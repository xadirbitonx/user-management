import PromiseRouter from "express-promise-router";
import {GroupsModule} from "../modules/groups/groups-module";


export function createGroupsRoute() {
    const groupsRoute = PromiseRouter();

    groupsRoute.post('/addGroup', async function (req, res, next) {
        const groupName: string = req.body['groupName'];

        await GroupsModule.addGroup(groupName);

        res.status(200).json();
    });

    return groupsRoute;
}
