import PromiseRouter from "express-promise-router";


export function createIndexRoute() {
    const indexRoute = PromiseRouter();

    indexRoute.get('/isAlive', function (req, res, next) {
        res.status(200).json("i am alive");
    });

    return indexRoute;
}
