import PromiseRouter from "express-promise-router";
import {IUser} from "../modules/users/users-model";
import {UsersModule} from "../modules/users/users-module";


export function createUsersRoute() {
    const usersRoute = PromiseRouter();

    usersRoute.get('/getUsers', async function (req, res, next) {
        const limit: number = req.body['limit'];
        const offset: number = req.body['offset'];

        try {
            const users: IUser[] = await UsersModule.getUsers(limit, offset);
            res.status(200).json(users);
        } catch (e) {
            res.status(500).json(e);
        }
    });
    usersRoute.get('/getUsersByName', async function (req, res, next) {
        const name: string = req.body['name'];

        try {
            const users: IUser[] = await UsersModule.getUsersByName(name);
            res.status(200).json(users);
        } catch (e) {
            res.status(500).json(e);
        }
    });
    usersRoute.get('/getUsersByEmail', async function (req, res, next) {
        const email: string = req.body['email'];

        try {
            const users: IUser[] = await UsersModule.getUsersByEmail(email);
            res.status(200).json(users);
        } catch (e) {
            res.status(500).json(e);
        }
    });
    usersRoute.get('/getUsersByGroup', async function (req, res, next) {
        const groupId: string = req.body['groupId'];

        try {
            const users: IUser[] = await UsersModule.getUsersByGroup(groupId);
            res.status(200).json(users);
        } catch (e) {
            res.status(500).json(e);
        }
    });
    usersRoute.post('/removeUserFromGroup', async function (req, res, next) {
        const userId: string = req.body['userId'];

        try {
            await UsersModule.removeUserFromGroup(userId);
            res.status(200).json();
        } catch (e) {
            res.status(500).json(e);
        }
    });
    usersRoute.post('/addUser', async function (req, res, next) {
        const user: IUser = req.body;

        await UsersModule.addUser(user);

        res.status(200).json();
    });

    return usersRoute;
}
